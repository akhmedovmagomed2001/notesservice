CREATE TABLE note (
	id SERIAL PRIMARY KEY,
	title VARCHAR(255) NOT NULL,
	text VARCHAR(1023) NOT NULL
);
