const configs_list = [
	"APP_PORT",
	"DB_HOST",
	"DB_PORT",
	"DB_NAME",
	"DB_USERNAME",
	"DB_PASSWORD",
	"REDIS_HOST",
	"REDIS_PORT",
	"RATE_LIMIT",
	"RATE_LIMIT_TIMEOUT_SEC",
];

const add_property = (map, property_key) => {
	const property_value = process.env[property_key];
	if (property_value === undefined || property_value === "") {
		throw new Error(`Property '${property_key}' is undefined!`);
	}
	map[property_key] = property_value;
	return map;
};

const configs = configs_list.reduce(add_property, {});

module.exports = configs;
