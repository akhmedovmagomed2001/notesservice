const database_pool = require("../config/database_config");

const GET_NOTES_QUERY = "SELECT * FROM note ORDER BY id ASC";
const GET_NOTE_BY_ID_QUERY = "SELECT * FROM note WHERE id = $1";
const DELETE_NOTE_QUERY = "DELETE FROM note WHERE id = $1";

const CREATE_NOTE_QUERY =
	" \
		INSERT INTO note (title, text) \
		VALUES ($1, $2) \
		RETURNING id \
	";

const UPDATE_NOTE_QUERY =
	" \
		UPDATE note \
		SET \
			title = $1, \
			text = $2 \
		WHERE id = $3 \
	";

const getNotes = (request, response) => {
	database_pool.query(GET_NOTES_QUERY, (error, results) => {
		if (error) {
			throw error;
		}
		response.status(200).json(results.rows);
	});
};

const getNoteById = (request, response) => {
	const id = parseInt(request.params.id);

	database_pool.query(GET_NOTE_BY_ID_QUERY, [id], (error, results) => {
		if (error) {
			throw error;
		}
		response.status(200).json(results.rows);
	});
};

const createNote = (request, response) => {
	const { title, text } = request.body;

	database_pool.query(CREATE_NOTE_QUERY, [title, text], (error, results) => {
		if (error) {
			throw error;
		}
		const inserted_entity_id = results.rows[0].id;

		const response_data = {
			message: `Note added with ID: ${inserted_entity_id}`,
		};

		response.status(201).json(response_data);
	});
};

const updateNote = (request, response) => {
	const id = parseInt(request.params.id);
	const { title, text } = request.body;

	database_pool.query(
		UPDATE_NOTE_QUERY,
		[title, text, id],
		(error, results) => {
			if (error) {
				throw error;
			}

			const response_data = {
				message: `Note modified with ID: ${id}`,
			};

			response.status(200).json(response_data);
		}
	);
};

const deleteNote = (request, response) => {
	const id = parseInt(request.params.id);

	database_pool.query(DELETE_NOTE_QUERY, [id], (error, results) => {
		if (error) {
			throw error;
		}

		const response_data = {
			message: `Note deleted with ID: ${id}`,
		};

		response.status(200).json(response_data);
	});
};

module.exports = {
	getNotes,
	getNoteById,
	createNote,
	updateNote,
	deleteNote,
};
