const log_request_middleware = (req, res, next) => {
	const path = req.originalUrl;
	const method = req.method;

	const message = `${method} ${path}`;
	console.log(message);

	next();
};

module.exports = log_request_middleware;
