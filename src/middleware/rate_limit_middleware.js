const redis_client = require("../config/redis_config");
const configs = require("../service/config_service");
const request_constants = require("../constants/request_constants");

const get_available_rate = async (user_id) => {
	let available_rate = await redis_client.get(user_id);

	if (available_rate === null) {
		available_rate = configs["RATE_LIMIT"];
		redis_client.set(user_id, configs["RATE_LIMIT"]);
		redis_client.expire(user_id, configs["RATE_LIMIT_TIMEOUT_SEC"]);
	}

	if (available_rate <= 0) {
		return available_rate;
	}

	available_rate--;
	redis_client.set(user_id, available_rate, { KEEPTTL: true });

	return available_rate + 1;
};

const rate_limit_middleware = async (req, res, next) => {
	const user_id = req.header(request_constants["USER_ID_HEADER"]);

	let available_rate = 1;
	if (user_id !== undefined) {
		available_rate = await get_available_rate(user_id);
	}

	if (available_rate <= 0) {
		const message = `User '${user_id}' has breached the available rate limit`;
		console.log(message);

		const response_data = { message: message };

		return res.status(429).json(response_data);
	}

	next();
};

module.exports = rate_limit_middleware;
