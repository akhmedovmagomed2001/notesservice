const notes_service = require("../service/notes_service");
const middlewares = require("../config/middleware_config");

/**
 * @swagger
 * components:
 *   schemas:
 *     Note:
 *       type: object
 *       required:
 *         - title
 *         - text
 *       properties:
 *         id:
 *           type: int
 *           description: The auto-generated id of the note
 *         title:
 *           type: string
 *           description: The title of the note
 *         text:
 *           type: string
 *           description: the content of the note
 *       example:
 *         id: 4
 *         title: Homework 9
 *         text: Unit 22, exercise 122, page 87
 * tags:
 *   name: Notes
 *   description: The notes managing API
 * /notes:
 *   get:
 *     summary: Lists all notes
 *     tags: [Notes]
 *     responses:
 *       200:
 *         description: The list of the notes
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/Note'
 *   post:
 *     summary: Create a new note
 *     tags: [Notes]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Note'
 *     responses:
 *       200:
 *         description: The created book.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Note'
 *       500:
 *         description: Some server error
 * /notes/{id}:
 *   get:
 *     summary: Get the note by id
 *     tags: [Notes]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The note id
 *     responses:
 *       200:
 *         description: The note response by id
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Note'
 *       404:
 *         description: The note was not found
 *   put:
 *    summary: Update the note by the id
 *    tags: [Notes]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: The note id
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Note'
 *    responses:
 *      200:
 *        description: The note was updated
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Note'
 *      404:
 *        description: The note was not found
 *      500:
 *        description: Some error happened
 *   delete:
 *     summary: Remove the note by id
 *     tags: [Notes]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The note id
 *
 *     responses:
 *       200:
 *         description: The note was deleted
 *       404:
 *         description: The note was not found
 */

const init = (app) => {
	app.get("/notes", middlewares, notes_service.getNotes);
	app.get("/notes/:id", middlewares, notes_service.getNoteById);
	app.post("/notes", middlewares, notes_service.createNote);
	app.put("/notes/:id", middlewares, notes_service.updateNote);
	app.delete("/notes/:id", middlewares, notes_service.deleteNote);
};

module.exports = { init };
