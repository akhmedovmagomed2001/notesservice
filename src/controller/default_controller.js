const init = (app) => {
	app.get("/", (request, response) => {
		response.json({
			message: "Hello!!! It's Node.js, Express, and Postgres API.",
		});
	});
};

module.exports = { init };
