const rate_limit_middleware = require("../middleware/rate_limit_middleware");
const log_request_middleware = require("../middleware/log_request_middleware");

const middlewares = [log_request_middleware, rate_limit_middleware];

module.exports = middlewares;
