const redis = require("redis");
const configs = require("../service/config_service");

const redis_host = configs["REDIS_HOST"];
const redis_port = configs["REDIS_PORT"];
const redis_url = `redis://${redis_host}:${redis_port}`;

const redis_client = redis.createClient({
	url: redis_url,
});

redis_client.connect();

module.exports = redis_client;
