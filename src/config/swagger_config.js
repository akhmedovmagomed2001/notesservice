const swaggerJsdoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const fs = require("fs");

const SWAGGER_CONFIG_FILEPATH = "resources/swagger_config.json";

const get_swagger_config = () => {
	const raw_data = fs.readFileSync(SWAGGER_CONFIG_FILEPATH);
	return JSON.parse(raw_data);
};

const config_swagger = (app) => {
	const config = get_swagger_config();
	const specs = swaggerJsdoc(config);

	app.use(
		"/api-docs",
		swaggerUi.serve,
		swaggerUi.setup(specs, { explorer: true })
	);
};

module.exports = config_swagger;
