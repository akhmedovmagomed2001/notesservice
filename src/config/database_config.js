const pg_db_driver = require("pg");
const configs = require("../service/config_service");

const database_pool = new pg_db_driver.Pool({
	host: configs['DB_HOST'],
	port: configs['DB_PORT'],
	database: configs['DB_NAME'],
	user: configs['DB_USERNAME'],
	password: configs['DB_PASSWORD'],
});

module.exports = database_pool;
