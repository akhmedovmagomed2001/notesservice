const express = require("express");
const bodyParser = require("body-parser");
const configs = require("../service/config_service");

const notes_controller = require("../controller/notes_controller");
const default_controller = require("../controller/default_controller");
const config_swagger = require("./swagger_config");

const init_application = () => {
	const app = express();

	app.use(bodyParser.json());
	app.use(
		bodyParser.urlencoded({
			extended: true,
		})
	);

	const port = configs['APP_PORT'];
	app.listen(port, () => {
		console.log(`App running on port ${port}.`);
	});

	notes_controller.init(app);
	default_controller.init(app);

	config_swagger(app);
};

module.exports = init_application;
